-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 01 Mar 2018 pada 02.41
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beasiswa`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `userId` varchar(12) DEFAULT NULL,
  `password` text,
  `idLevel` int(11) DEFAULT NULL,
  `status` enum('open','close') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `userId`, `password`, `idLevel`, `status`) VALUES
(25, '1', '1', 1, 'open'),
(26, '2', '2', 2, 'open'),
(27, '3', '3', 3, 'open'),
(28, '4', '4', 4, 'open'),
(29, '5', '5', 5, 'open'),
(34, '6', '6', 6, 'open'),
(35, '15650025', '15650025', 5, 'open'),
(36, '15650026', '15650026', 5, 'open');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bea`
--

CREATE TABLE `bea` (
  `id` int(11) NOT NULL,
  `namaBeasiswa` varchar(100) DEFAULT NULL,
  `penyelenggaraBea` varchar(100) DEFAULT NULL,
  `kuota` char(10) DEFAULT NULL,
  `beasiswaDibuka` date DEFAULT NULL COMMENT 'pendaftaran',
  `beasiswaTutup` date DEFAULT NULL COMMENT 'pendaftaran',
  `periodeBerakhir` date DEFAULT NULL,
  `statusBeasiswa` enum('2','3') DEFAULT '2' COMMENT '2=ditolak, 3=dikonfirmasi',
  `seleksiTutup` date NOT NULL,
  `selektor` int(1) NOT NULL COMMENT '1=kasubag kemahasiswaa, 2=kasubag fakultas, 3=keduanya',
  `selektorFakultas` int(5) DEFAULT NULL COMMENT 'berisi idFakultas',
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bea`
--

INSERT INTO `bea` (`id`, `namaBeasiswa`, `penyelenggaraBea`, `kuota`, `beasiswaDibuka`, `beasiswaTutup`, `periodeBerakhir`, `statusBeasiswa`, `seleksiTutup`, `selektor`, `selektorFakultas`, `keterangan`) VALUES
(17, 'Bank Indonesia 2018', 'Bank Indonesia', '50', '2018-02-25', '2018-03-01', '2019-02-25', '3', '2018-03-05', 1, NULL, NULL),
(18, 'Bank BRI', 'Bank BRI', '50', '2018-02-25', '2018-03-01', '2019-02-25', '3', '2018-03-05', 1, NULL, NULL),
(19, 'Bank BTN', 'Bank BTN', '50', '2018-02-25', '2018-03-01', '2019-02-25', '3', '2018-03-05', 1, NULL, NULL),
(20, 'Beasiswa Supersemar', 'Beasiswa Supersemar', '50', '2018-02-25', '2018-03-01', '2019-02-25', '3', '2018-03-05', 1, NULL, NULL),
(22, 'Beasiswa BidikMisi', 'Pemerintah Indonesia', '40', '2018-02-25', '2018-03-03', '2018-01-31', '3', '2018-03-05', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `idBerita` int(11) NOT NULL,
  `judulBerita` text,
  `topikBerita` text,
  `penulisBerita` varchar(100) DEFAULT NULL,
  `kontenBerita` text,
  `tglInBerita` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`idBerita`, `judulBerita`, `topikBerita`, `penulisBerita`, `kontenBerita`, `tglInBerita`) VALUES
(1, 'SI BEA', 'Sistem Informasi Pendaftaran Beasiswa UIN Malang', 'Iqbal Fauzi', '<p xss=removed>Pada penghujung akhir tahun 2017, Subbag. Kemahasiswaan meluncurkan sistem informasi penerimaan beasiswa baru yang diberi nama <strong>SIBEA</strong>, sebagai pengembangan dari sistem sebelumnya. Pada sistem ini terjadi banyak perubahan baik dari segi Layout, manajemen penerimaan, user level, dan juga report yang ada.</p>\r\n\r\n<p xss=removed>Harapan kedepannya semoga sistem yang baru dapat membawa kemudahan pengelolaan, lebih cepat pengelolaan, transparansi, dan kemudahan akses bagi civitas akademika UIN Maulana Malik Ibrahim Malang.</p>\r\n\r\n<p xss=removed>Kritik dan saran yang membangun sangat kami harapkan dari seluruh civitas akademika UIN Maulana MalikIbrahim Malang, guna meningkatkan pelayanan kami pada semua.</p>\r\n', '2018-02-27 17:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(11) NOT NULL,
  `namaFk` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `fakultas`
--

INSERT INTO `fakultas` (`id`, `namaFk`) VALUES
(1, 'Tarbiyah dan Ilmu Keguruan'),
(2, 'Syari’ah'),
(3, 'Humaniora'),
(4, 'Psikologi'),
(5, 'Ekonomi'),
(6, 'Sains dan Teknologi'),
(7, 'Kedokteran');

-- --------------------------------------------------------

--
-- Struktur dari tabel `identitas_mhs`
--

CREATE TABLE `identitas_mhs` (
  `nimMhs` int(11) NOT NULL,
  `namaLengkap` varchar(100) DEFAULT NULL,
  `tempatLahir` text,
  `tglLahir` date DEFAULT NULL,
  `asalKota` varchar(20) DEFAULT NULL,
  `namaOrtu` varchar(20) DEFAULT NULL,
  `alamatOrtu` text,
  `kotaOrtu` varchar(30) DEFAULT NULL,
  `propinsiOrtu` varchar(30) DEFAULT NULL,
  `angkatan` char(5) DEFAULT NULL,
  `jenisKel` tinyint(1) DEFAULT NULL,
  `alamatLengkap` text,
  `emailAktif` varchar(100) DEFAULT NULL,
  `noTelp` char(12) DEFAULT NULL,
  `fotoMhs` varchar(100) DEFAULT NULL,
  `idJrs` int(11) DEFAULT NULL,
  `idAkses` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `identitas_mhs`
--

INSERT INTO `identitas_mhs` (`nimMhs`, `namaLengkap`, `tempatLahir`, `tglLahir`, `asalKota`, `namaOrtu`, `alamatOrtu`, `kotaOrtu`, `propinsiOrtu`, `angkatan`, `jenisKel`, `alamatLengkap`, `emailAktif`, `noTelp`, `fotoMhs`, `idJrs`, `idAkses`) VALUES
(5, 'Iqbal fauzi', 'Malang', '2018-02-28', 'Malang', 'Ali Bahrodin', 'Malang', 'Malang', 'Jawa Timur', '2005', NULL, 'Malang', 'iqbalfauzi110@gmail.com', '085765768975', NULL, 2, 29),
(15650024, 'op', 'op', '2017-10-11', 'op', 'op', 'op', 'op', 'op', '2015', NULL, 'op', 'iqbalfauzi100@gmail.com', '89', NULL, 2, 35),
(15650025, 'Iqbal Fauzi', 'Malang', '2017-10-25', 'Malang', 'Ali', 'Malang', 'Malang', 'Jawa Timur', '2015', NULL, 'Malang', 'iqbal@gmail.com', '085765478765', 'file_1519075656.jpg', 2, 35),
(15650026, 'Achmad Wibowo', 'Malang', '2017-08-01', 'Malang', 'Muh', 'Malang', 'Malang', 'Jawa Timur', '2017', 1, 'Malang', 'Muhammad@gmail.com', '085756787654', 'file_1503562985.png', 6, 29),
(15650027, 'Muhammad', 'Malang', '2017-08-01', 'Malang', 'Muh', 'Malang', 'Malang', 'Jawa Timur', '2017', 1, 'Malang', 'Muhammad@gmail.com', '085756787654', 'file_1503562985.png', 6, 29),
(15650028, 'qwerty', 'werty', '2017-10-17', 'werty', 'werty', 'werty', 'werty', 'werty', '2015', NULL, 'werty', 'iqbalfauzi100@gmail.com', '09876', NULL, NULL, NULL),
(15650029, 'asdfghjkl', 'asdfghjkl', '2017-10-11', 'asdfghjkl', 'asdfghjkl', 'asdfghjkl', 'asdfghjkl', 'asdfghjkl', '2015', NULL, 'asdfghjkl', 'asdfghjkl', '0987', NULL, NULL, NULL),
(15650030, 'Fauzi', 'Fauzi', '2017-10-20', 'Fauzi', 'Fauzi', 'Fauzi', 'Fauzi', 'Fauzi', '2015', NULL, 'Fauzi', 'iqbal', '0987654321', 'file_1507734281.png', NULL, NULL),
(15650031, 'polo', 'polo', '2017-10-01', 'polo', 'polo', 'polo', 'polo', 'polo', '2015', NULL, 'polo', 'polo@gmail.com', '098', 'file_1507734875.png', NULL, NULL),
(15650032, 'asu', 'asu', '2017-10-11', 'asu', 'asu', 'asu', 'asu', 'asu', '2015', NULL, 'asu', 'iqbalfauzi100@gmail.com', '0987', 'file_1507736222.png', NULL, NULL),
(15650033, 'as', 'as', '2017-10-11', 'as', 'as', 'as', 'as', 'as', '2015', NULL, 'as', 'as', '09', 'file_1507736488.png', NULL, NULL),
(15650034, 'opo', 'opo', '2017-10-11', 'opo', 'opo', 'opo', 'opo', 'opo', '2015', NULL, 'opo', 'iqbalfauzi100@gmail.com', '0987', NULL, NULL, 35),
(15650035, 'tt', 'tt', '2017-10-11', 'tt', 'tt', 'tt', 'tt', 'tt', '2015', NULL, 'tt', 'youpie', '09', 'file_1507737629.gif', NULL, 35),
(15650036, 'yuyu', 'yuyu', '2017-10-11', 'yu', 'yu', 'yu', 'yu', 'yu', '2015', NULL, 'yu', 'yu', '89', 'file_1507738577.png', NULL, 35);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(11) NOT NULL,
  `namaJur` varchar(100) DEFAULT NULL,
  `idFk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jurusan`
--

INSERT INTO `jurusan` (`id`, `namaJur`, `idFk`) VALUES
(2, 'Teknik Informatika', 6),
(3, 'Pendidikan Agama Islam', 1),
(4, 'Pendidikan IPS', 1),
(5, 'Pendidikan Guru Madrasah Ibtidaiyah (PGMI)', 1),
(6, 'Pendidikan Bahasa Arab', 1),
(7, 'Pendidikan Islam Anak Usia Dini (PIAUD)', 1),
(8, 'Manajemen Pendidikan Islam', 1),
(9, 'Al-Ahwal Al-Syakhsiyah', 2),
(10, 'Hukum Bisnis Syari\'ah', 2),
(11, 'Hukum Tata Negara', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_skor`
--

CREATE TABLE `kategori_skor` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_skor`
--

INSERT INTO `kategori_skor` (`id`, `nama`) VALUES
(1, 'PLN'),
(2, 'Penghasilan Ayah'),
(3, 'IPK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `levellog`
--

CREATE TABLE `levellog` (
  `id` int(11) NOT NULL,
  `level` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `levellog`
--

INSERT INTO `levellog` (`id`, `level`) VALUES
(1, 'Staff Kemahasiswaan'),
(2, 'Kasubag'),
(3, 'Kasubag Fakultas'),
(4, 'Kabag'),
(5, 'Mahasiswa'),
(6, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftar`
--

CREATE TABLE `pendaftar` (
  `id` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `semester` int(1) NOT NULL,
  `sks` int(2) NOT NULL,
  `ipk` float NOT NULL,
  `alamatMalang` text NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '1=diterima, 0=tidak_diterima',
  `waktuDiubah` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pendaftar`
--

INSERT INTO `pendaftar` (`id`, `idBea`, `nim`, `semester`, `sks`, `ipk`, `alamatMalang`, `tanggal`, `status`, `waktuDiubah`) VALUES
(6, 22, 15650025, 7, 3, 3, 'Malang', '2018-02-28', 0, '2018-02-28 18:53:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendaftar_skor`
--

CREATE TABLE `pendaftar_skor` (
  `id` int(40) NOT NULL,
  `idPendaftar` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `idSubKategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pendaftar_skor`
--

INSERT INTO `pendaftar_skor` (`id`, `idPendaftar`, `idBea`, `idKategori`, `idSubKategori`) VALUES
(16, 6, 22, 1, 2),
(17, 6, 22, 2, 8),
(18, 6, 22, 3, 11);

-- --------------------------------------------------------

--
-- Stand-in structure for view `penerima_bea`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `penerima_bea` (
`nimMhs` int(11)
,`namaLengkap` varchar(100)
,`idBea` int(11)
,`namaBeasiswa` varchar(100)
,`ipk` float
,`skor` decimal(32,0)
,`jumlah` double
,`status` int(1)
,`beasiswaDibuka` date
,`beasiswaTutup` date
,`periodeBerakhir` date
,`now` date
,`berakhirPendaftaran` int(7)
,`berakhirPeriode` int(7)
,`updated` timestamp
);

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil_admin`
--

CREATE TABLE `profil_admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `noTelp` char(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `idAkses` int(11) DEFAULT NULL,
  `idFakultas` int(11) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `profil_admin`
--

INSERT INTO `profil_admin` (`id`, `nama`, `alamat`, `noTelp`, `email`, `idAkses`, `idFakultas`, `foto`) VALUES
(31, 'Iqbal Fauzi', 'Malang', '085735059741', 'iqbal@gmail.com', 25, NULL, 'file_1519074843.jpg'),
(32, 'Tarbiyah', 'Malang', '085746987543', 'tarbiyah@gmail.com', 27, 6, 'file_1519075282.png'),
(33, 'admin', 'admin', '099', 'admin@gmail.com', 34, NULL, 'file_1519075718.jpg'),
(34, 'kasubag', 'kasubag', '099', 'kasubag@gmail.com', 26, NULL, 'file_1519075869.png'),
(35, 'kabag', 'kabag', '099', 'kabag@gmail.com', 28, NULL, 'file_1519076852.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_bea_kategori_skor`
--

CREATE TABLE `set_bea_kategori_skor` (
  `id` int(11) NOT NULL,
  `idBea` int(11) NOT NULL,
  `idKategoriSkor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `set_bea_kategori_skor`
--

INSERT INTO `set_bea_kategori_skor` (`id`, `idBea`, `idKategoriSkor`) VALUES
(17, 22, 1),
(18, 22, 2),
(19, 22, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_sub_kategori_skor`
--

CREATE TABLE `set_sub_kategori_skor` (
  `id` int(11) NOT NULL,
  `idKategoriSkor` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL,
  `skor` int(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `set_sub_kategori_skor`
--

INSERT INTO `set_sub_kategori_skor` (`id`, `idKategoriSkor`, `nama`, `skor`) VALUES
(1, 1, '<450', 1),
(2, 1, '450-750', 2),
(3, 1, '750-900', 3),
(4, 1, '900-1000', 4),
(8, 2, '700-900rb', 3),
(9, 2, '900-1000rb', 4),
(10, 3, '1-4', 1),
(11, 3, '2-3', 2),
(12, 3, '3-4', 3);

-- --------------------------------------------------------

--
-- Stand-in structure for view `skor_mahasiswa`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `skor_mahasiswa` (
`nimMhs` int(11)
,`namaLengkap` varchar(100)
,`idPendaftar` int(11)
,`idBeasiswa` int(11)
,`namaBeasiswa` varchar(100)
,`ipk` float
,`skor` decimal(32,0)
,`jumlah` double
,`status` int(1)
,`updated` timestamp
);

-- --------------------------------------------------------

--
-- Struktur untuk view `penerima_bea`
--
DROP TABLE IF EXISTS `penerima_bea`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `penerima_bea`  AS  select `pendaftar`.`nim` AS `nimMhs`,`identitas_mhs`.`namaLengkap` AS `namaLengkap`,`pendaftar`.`idBea` AS `idBea`,`bea`.`namaBeasiswa` AS `namaBeasiswa`,`pendaftar`.`ipk` AS `ipk`,(select sum(`set_sub_kategori_skor`.`skor`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where (`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`)) AS `skor`,(select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where (`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`)) AS `jumlah`,`pendaftar`.`status` AS `status`,`bea`.`beasiswaDibuka` AS `beasiswaDibuka`,`bea`.`beasiswaTutup` AS `beasiswaTutup`,`bea`.`periodeBerakhir` AS `periodeBerakhir`,curdate() AS `now`,(to_days(curdate()) - to_days(`bea`.`beasiswaTutup`)) AS `berakhirPendaftaran`,(to_days(curdate()) - to_days(`bea`.`periodeBerakhir`)) AS `berakhirPeriode`,`pendaftar`.`waktuDiubah` AS `updated` from ((`pendaftar` left join `bea` on((`pendaftar`.`idBea` = `bea`.`id`))) left join `identitas_mhs` on((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`))) where ((`pendaftar`.`status` = '1') and (year(`bea`.`beasiswaTutup`) between (year(curdate()) - 1) and (year(curdate()) + 1)) and (year(`bea`.`periodeBerakhir`) between (year(curdate()) - 1) and (year(curdate()) + 1))) order by (select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where (`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`)) desc ;

-- --------------------------------------------------------

--
-- Struktur untuk view `skor_mahasiswa`
--
DROP TABLE IF EXISTS `skor_mahasiswa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `skor_mahasiswa`  AS  select `pendaftar`.`nim` AS `nimMhs`,`identitas_mhs`.`namaLengkap` AS `namaLengkap`,`pendaftar`.`id` AS `idPendaftar`,`pendaftar`.`idBea` AS `idBeasiswa`,`bea`.`namaBeasiswa` AS `namaBeasiswa`,`pendaftar`.`ipk` AS `ipk`,(select sum(`set_sub_kategori_skor`.`skor`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where ((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`) and (`pendaftar`.`idBea` = `idBeasiswa`))) AS `skor`,(select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where ((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`) and (`pendaftar`.`idBea` = `idBeasiswa`))) AS `jumlah`,`pendaftar`.`status` AS `status`,`pendaftar`.`waktuDiubah` AS `updated` from ((`pendaftar` left join `bea` on((`pendaftar`.`idBea` = `bea`.`id`))) left join `identitas_mhs` on((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`))) order by (select (sum(`set_sub_kategori_skor`.`skor`) + `pendaftar`.`ipk`) from (((`pendaftar_skor` left join `pendaftar` on((`pendaftar`.`id` = `pendaftar_skor`.`idPendaftar`))) left join `kategori_skor` on((`pendaftar_skor`.`idKategori` = `kategori_skor`.`id`))) left join `set_sub_kategori_skor` on((`pendaftar_skor`.`idSubKategori` = `set_sub_kategori_skor`.`id`))) where ((`pendaftar`.`nim` = `identitas_mhs`.`nimMhs`) and (`pendaftar`.`idBea` = `idBeasiswa`))) desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_level` (`idLevel`);

--
-- Indexes for table `bea`
--
ALTER TABLE `bea`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`idBerita`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identitas_mhs`
--
ALTER TABLE `identitas_mhs`
  ADD PRIMARY KEY (`nimMhs`),
  ADD UNIQUE KEY `nimMhs` (`nimMhs`),
  ADD KEY `jurusan-mhs` (`idJrs`),
  ADD KEY `id_akses` (`idAkses`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurusan_fk` (`idFk`);

--
-- Indexes for table `kategori_skor`
--
ALTER TABLE `kategori_skor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levellog`
--
ALTER TABLE `levellog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nim` (`nim`),
  ADD KEY `idBea` (`idBea`);

--
-- Indexes for table `pendaftar_skor`
--
ALTER TABLE `pendaftar_skor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idPendaftar` (`idPendaftar`),
  ADD KEY `idBea` (`idBea`),
  ADD KEY `idKategori` (`idKategori`),
  ADD KEY `idSubKategori` (`idSubKategori`);

--
-- Indexes for table `profil_admin`
--
ALTER TABLE `profil_admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_akses` (`idAkses`);

--
-- Indexes for table `set_bea_kategori_skor`
--
ALTER TABLE `set_bea_kategori_skor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_bea` (`idBea`),
  ADD KEY `id_kategori_skor` (`idKategoriSkor`);

--
-- Indexes for table `set_sub_kategori_skor`
--
ALTER TABLE `set_sub_kategori_skor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_kategori_skor` (`idKategoriSkor`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `bea`
--
ALTER TABLE `bea`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `idBerita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `kategori_skor`
--
ALTER TABLE `kategori_skor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `levellog`
--
ALTER TABLE `levellog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pendaftar`
--
ALTER TABLE `pendaftar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pendaftar_skor`
--
ALTER TABLE `pendaftar_skor`
  MODIFY `id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `profil_admin`
--
ALTER TABLE `profil_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `set_bea_kategori_skor`
--
ALTER TABLE `set_bea_kategori_skor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `set_sub_kategori_skor`
--
ALTER TABLE `set_sub_kategori_skor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD CONSTRAINT `akses_ibfk_1` FOREIGN KEY (`idLevel`) REFERENCES `levellog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `identitas_mhs`
--
ALTER TABLE `identitas_mhs`
  ADD CONSTRAINT `identitas_mhs_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jurusan-mhs` FOREIGN KEY (`idJrs`) REFERENCES `jurusan` (`id`);

--
-- Ketidakleluasaan untuk tabel `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_fk` FOREIGN KEY (`idFk`) REFERENCES `fakultas` (`id`);

--
-- Ketidakleluasaan untuk tabel `pendaftar`
--
ALTER TABLE `pendaftar`
  ADD CONSTRAINT `pendaftar_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `identitas_mhs` (`nimMhs`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_ibfk_2` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `pendaftar_skor`
--
ALTER TABLE `pendaftar_skor`
  ADD CONSTRAINT `pendaftar_skor_ibfk_1` FOREIGN KEY (`idPendaftar`) REFERENCES `pendaftar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_skor_ibfk_2` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_skor_ibfk_3` FOREIGN KEY (`idKategori`) REFERENCES `kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pendaftar_skor_ibfk_4` FOREIGN KEY (`idSubKategori`) REFERENCES `set_sub_kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `profil_admin`
--
ALTER TABLE `profil_admin`
  ADD CONSTRAINT `profil_admin_ibfk_1` FOREIGN KEY (`idAkses`) REFERENCES `akses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `set_bea_kategori_skor`
--
ALTER TABLE `set_bea_kategori_skor`
  ADD CONSTRAINT `set_bea_kategori_skor_ibfk_1` FOREIGN KEY (`idBea`) REFERENCES `bea` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `set_bea_kategori_skor_ibfk_2` FOREIGN KEY (`idKategoriSkor`) REFERENCES `kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `set_sub_kategori_skor`
--
ALTER TABLE `set_sub_kategori_skor`
  ADD CONSTRAINT `set_sub_kategori_skor_ibfk_1` FOREIGN KEY (`idKategoriSkor`) REFERENCES `kategori_skor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
