<main>
  <div class="container">
    <h4><span class="blue-text">Pengaturan Kriteria AHP</span></h4>
    <div id="dashboard">
      <div class="section" id="responsive">
        <div class="row">
          <div class="col s12 m6">
            <button class="waves-effect waves-light btn green" onclick="add_data()" title="Panduan Penilaian Kriteria"><i class="mdi-image-loupe"></i> Panduan Penilaian Kriteria</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>


<script type="text/javascript">

  var save_method;
  var arr = 0;
  var dataTable;

  document.addEventListener("DOMContentLoaded", function(event) {
    datatable();
  });

  function datatable() {
    dataTable = $('#tabel').DataTable({
      "processing":true,
      "serverSide":true,
      "order":[],
      "ajax":{
        url:"<?php echo base_url('kasubag/C_master_bobot_kriteria/datatable'); ?>",
        type:"POST"
      },
      "columnDefs":[
      {
        "targets":[2,-1],
        "orderable":false,
      },
      ],
      "dom": '<"row" <"col s6 m6 l3 left"l><"col s6 m6 l3 right"f>><"bersih tengah" rt><"bottom"ip>',
      language : {
        sLengthMenu: "Tampilkan _MENU_",
        sSearch: "Cari:"
      }
    });
  }

  function reload_table(){
    dataTable.ajax.reload(null,false);
  }

  function add_data() {
    arr = 0;
    save_method = 'add';
    $('#modal1').openModal();
  }

  function save()
  {
    var url;
    if(save_method == 'add')
    {
      url = "<?php echo site_url('kasubag/C_master_bobot_kriteria/add_data')?>";
    }
    else
    {
      url = "<?php echo site_url('kasubag/C_master_bobot_kriteria/update_data')?>";
    }
    $.ajax({
      url : url,
      type: "POST",
      data: $('#formInput').serialize(),
      dataType: "JSON",
      success: function(data)
      {
       $('#modal1').closeModal();
       reload_table();
     },
     error: function (jqXHR, textStatus, errorThrown)
     {
      alert('Error adding/update data');
    }
  });
  }

  function edit(id) {
    arr = 0;
    save_method = 'update';
    $('#formInput')[0].reset();
    $('#scoreInput').empty();

    $.ajax({
      url : "<?php echo site_url('kasubag/C_master_bobot_kriteria/edit_data/')?>/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('#jenisScoring').val(data[0].namaJenis);
        $('#idJenisScoring').val(data[0].idJenis);

        for (var i = 1; i < data.length; i++) {
          idSub = data[i].idSub;
          sub = data[i].sub;
          skor = data[i].skor;
          score_input = `
          <div class="input-field col s10">
            <input type="hidden" id="idSub[`+arr+`]" name="idSub[`+arr+`]" value="`+idSub+`" class="validate">
            <input name="score[`+arr+`]" id="score[`+arr+`]" type="text" value="`+sub+`">
            <label for="score[`+arr+`]">Scoring `+(arr+1)+`</label>
          </div>
          <div class="input-field col s2">
            <input name="bobot[`+arr+`]" id="bobot[`+arr+`]" type="text" value="`+skor+`" class="validate">
            <label for="bobot[`+arr+`]">Bobot<label>
            </div>
            `;
            $("#scoreInput").append(score_input);
            arr+=1;
          }
          reloadJs('materialize', 'min');
          reloadJs('initialize', 'nomin');
          $('#modal1').openModal();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          alert('Error get data');
        }
      });
  }

  function remove(id, nama) {
    swal({
      title: '"'+nama+'"',
      text: "Apakah anda yakin ingin menghapus jenis scoring ini?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: '#F44336',
      confirmButtonText: 'Hapus',
      cancelButtonText: "Batal",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm){
      if (isConfirm){
        $.ajax({
          url : "<?php echo site_url('kasubag/C_master_bobot_kriteria/delete_data/')?>/"+id,
          type: "POST",
          dataType: "JSON",
          success: function(data)
          {
            reload_table();
            swal("Terhapus :(", "Jenis scoring pilihan telah berhasil dihapus!", "success");
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            swal("Erorr!", "Terjadi masalah saat penghapusan data!", "error");
          }
        });
      } else {
        swal("Dibatalkan :)", "Penghapusan jenis scoring dibatalkan!", "error");
      }
    });
  }
</script>

<!-- Modal Structure -->
<div id="modal1" class="modal">
  <div class="modal-content">
  <h4 class="modal-title"><span class="blue-text"><i class="mdi-image-loupe"></i> Tabel Panduan Penilaian Kriteria</span></h4>
    <hr>
    <p>Masing-masing kolom diisi dengan angka antara 1 s/d 9, dimana angka tersebut adalah nilai dari perbandingan berpasangan antar kriteria.</p>
    <style>
      table#panduan_tabel>tbody>tr>td:first-child{text-align:center}
      .rd{color:rgb(237,28,36);}
      .bl{color:rgb(0,162,232);}
    </style>
    <table class="bordered striped" id="panduan_tabel">
      <thead>
       <tr>
        <th>Nilai</th>
        <th>Keterangan</th>
      </tr>
    </thead>
    <tbody>
     <tr>
      <td>1</td>
      <td>Kedua kriteria <strong>sama pentingnya</strong></td>
    </tr>
    <tr>
      <td>3</td>
      <td>Salah satu kriteria <strong>sedikit lebih penting</strong></td>
    </tr>
    <tr>
      <td>5</td>
      <td>Salah satu kriteria <strong>jelas lebih penting</strong></td>
    </tr>
    <tr>
      <td>7</td>
      <td>Salah satu kriteria <strong>sangat jelas lebih penting</strong></td>
    </tr>
    <tr>
      <td>9</td>
      <td>Salah satu kriteria <strong>paling lebih penting</strong></td>
    </tr>
    <tr>
      <td>2,4,6,8</td>
      <td>Digunakan apabila ragu-ragu diantara dua nilai yang berdekatan</strong></td>
    </tr>
    <tr>
      <td>Kebalikan (1/Nilai)</td>
      <td>Apabila satu kriteria mendapat sebuah nilai, maka kriteria pasangannya mendapat kebalikannya (1/nilai)</td>
    </tr>
  </tbody>
</table><br>
<h6><span class="brown-text"><b>Contoh</b></span></h6>
<div style="text-align:center"><img src="<?= base_url('assets/img/nilai-kr.jpg') ?>" alt="contoh"/></div>
<p>Anda mengisi nilai perbandingan kriteria antara <span class='rd'><strong>Indeks Prestasi Akademik (IPK)</strong></span> dan <span class='bl'><strong>Penghasilan Ayah</strong></span>. Apabila <span class='rd'><strong>IPK</strong></span> <strong>sedikit lebih penting</strong> dari <span class='bl'><strong>Penghasilan Ayah</strong></span>, maka kolom diisi nilai <strong>3</strong>. Sebaliknya, apabila <span class='bl'><strong>Penghasilan Ayah</strong></span> <strong>sedikit lebih penting</strong> dari <span class='rd'><strong>IPK</strong></span>, maka diisi dengan <strong>1/3</strong></p>
<p><span class="glyphicon glyphicon-info-sign"></span> Perhatikan untuk mengisi nilai kriteria perbandingan berpasangan dengan urutan pasangan <span class='rd'><strong>Baris</strong></span> lalu <span class='bl'><strong>Kolom</strong></span></p>
</div>
<div class="modal-footer">
  <button class="modal-action modal-close waves-effect red btn"><i class="mdi-navigation-cancel left"></i> Tutup</button>
</div>
</div>